EXPRESSCHECKOUT WITH PAYPAL DEMO

1) Download and install an Apache Tomcat (version >= 5.5.25) link: http://tomcat.apache.org/
2) Browse to the webapp directory of Apache Tomcat. Unzip the downloaded demo code folder and place it in this webapp directory.
3) Start the Apache Tomcat server in XAMPP .  
4) Open the website in the browser and access it as: http://my_domain/java_code_folder_name/index.jsp
   Here, my_domain will be localhost if hosting on your own machine.  
   The java_code_folder_name is the name of the folder under which the downloaded code resides ('checkout' folder in case you have not changed the default name).